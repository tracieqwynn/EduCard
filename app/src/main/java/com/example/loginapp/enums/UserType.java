package com.example.loginapp.enums;

public enum UserType {
    /* User Privilege */
    Student,
    Teacher,
    Admin
}

package com.example.loginapp.enums;

public enum Gender {
    /* Default Genders */
    Female,
    Male
}

package com.example.loginapp.boundaries;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.loginapp.R;

// Show navigation and user profile details in this page
public class HomePage extends AppCompatActivity {

    // Tagged to home page UIs

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        // Actions in home page

    }

    // Methods in home page
}